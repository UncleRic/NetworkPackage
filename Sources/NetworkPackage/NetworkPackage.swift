
import Foundation

enum HTTPError: Error {
    case urlFailded
    case noData
    case requestError
    case parsingFailed
}

public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
    public static let get = HTTPMethod(rawValue: "GET")
    public static let post = HTTPMethod(rawValue: "POST")
    public let rawValue: String
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

// ==============================================================================================

public struct NetworkPackage {
    public private(set) var text = "Hello, World!"
    public init() {}
}

// ==============================================================================================

public class ServiceManager {
    var name: String?
    public static let shared = ServiceManager()

    public func callService<T: Decodable>(urlString: String, success: @escaping ((T) -> Void), fail: @escaping (() -> Void)) {
        let url = URL(string: urlString)
        guard let url = url else {
            fail()
            return
        }

        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest, completionHandler: { data, _, error in

            guard let data = data, error == nil else {
                fail()
                return
            }

            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            if let json = try? decoder.decode(T.self, from: data) {
                success(json)
            } else {
                fail()
            }

        })
        task.resume()
    }
}
